let () =
  let _ = Arkworks.Fr.one in
  let _ = Arkworks.Fr.zero in
  let a = Arkworks.Fr.random () in
  let b = Arkworks.Fr.random () in
  let res = Arkworks.Fr.(add zero one) in
  assert (Arkworks.Fr.(eq one one)) ;
  assert (Arkworks.Fr.(eq one res)) ;
  assert (Arkworks.Fr.(eq (add a b) (add b a)))
