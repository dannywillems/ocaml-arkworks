(*
   https://zshipko.github.io/ocaml-rs/02_type_conversion.html#types-that-work-directly-on-ocaml-values


      OCaml Memory                     Rust
           |
    | value a : t |
          |   points to (no copy)
          |-------------------------> value v : t'

   So I am concerned about this. There will be an automatic conversion. It is
   not simply a pointer to a rust value
   I don't like that.
   There will be copies under the hood, and it will slow down the binding.
   Might be negligeable for computational intensive functions, but not for low
   level arithmetic operations like the ones on Fr
   What we want is to allocate on the Caml side a simply value of type t, which
   is therefore managed by the OCaml GC, and the value of type t contains a Rust
   value. No memory copy must be performed. Performance is key!

   Ah! https://zshipko.github.io/ocaml-rs/02_type_conversion.html#wrapping-rust-values
   That can be interesting

   Rust values can be used as opaque values that can be shared with OCaml using
   ocaml::Pointer. The Pointer type allows for Rust values to be allocated using
   the OCaml runtime, this means their lifetime will be handled by the garbage
   collector. Pointer::alloc_final is used to move an existing Rust type into an
   OCaml allocated pointer, but even better is the option to implement the Custom
   trait for your type.

   Implementing Custom allows you to define equality/comparison, finalization,
   hashing and serialization functions for your type that will be used by OCaml.
   When allocating custom values you should use Pointer::from or
   Pointer::alloc_custom.
*)

module Stubs = struct
  type t

  external arkworks_zero : unit -> t = "caml_rs_bls12_381_zero"

  external arkworks_one : unit -> t = "caml_rs_bls12_381_one"

  external arkworks_allocate : unit -> t = "caml_rs_bls12_381_allocate"

  external arkworks_random : unit -> t = "caml_rs_bls12_381_random"

  external arkworks_add : t -> t -> t -> unit = "caml_rs_bls12_381_add"

  external arkworks_eq : t -> t -> bool = "caml_rs_bls12_381_eq"
end

(* type t is defined in rust *)
module type S = sig
  type t

  val zero : t

  val one : t

  val random : unit -> t

  val add : t -> t -> t

  val eq : t -> t -> bool
end

module Fr : S = struct
  type t = Stubs.t

  let eq x y = Stubs.arkworks_eq x y

  let allocate () = Stubs.arkworks_allocate ()

  let random () = Stubs.arkworks_random ()

  let one = Stubs.arkworks_one ()

  let zero = Stubs.arkworks_zero ()

  let add x y =
    let res = allocate () in
    Stubs.arkworks_add res x y ;
    res
end
(* Lets start with a simple binding to integers *)
