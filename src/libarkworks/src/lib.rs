use ark_bls12_381::Fr;
use ark_ff::UniformRand;
use ark_std::{ops::AddAssign, test_rng};

struct CRFr(ark_bls12_381::Fr);
ocaml::custom!(CRFr);
use ocaml::Pointer;

/*
                                                     |
         ret                                                     |
    ------------------------------------------
   |               Block Caml                | (8 bytes)   Caml heap
   |  (allocated with caml_alloc_custom_mem) |
   -------------------------------------------                   |
        |                                                        |
 ----------------------------------------------------------------------------
        |                                                        |
   ---------------------
       Rust Struct (CamlFr)
   ---------------------
        |
   ----------------------
   |  ark_bls12_381::Fr a|      64 * 4 = 256 bits             Rust heap
   ----------------------
         ^
         |
         Blst_fr_val(ret)
*/

// H: buffer is initialized to zero :)
#[ocaml::func]
pub unsafe fn caml_rs_bls12_381_add(
    mut vbuffer: ocaml::Pointer<CRFr>,
    vx: ocaml::Pointer<CRFr>,
    vy: ocaml::Pointer<CRFr>,
) {
    let x = vx.as_ref().0;
    let y = vy.as_ref().0;
    let mut buffer = vbuffer.as_ref().0;
    buffer = x + y;
    vbuffer.set(CRFr(buffer))
}

#[ocaml::func]
pub unsafe fn caml_rs_bls12_381_random() -> ocaml::Pointer<CRFr> {
    let mut rng = test_rng();
    ocaml::Pointer::alloc_custom(CRFr(Fr::rand(&mut rng)))
}

#[ocaml::func]
pub unsafe fn caml_rs_bls12_381_allocate() -> ocaml::Pointer<CRFr> {
    ocaml::Pointer::alloc_custom(CRFr(Fr::from(0)))
}

#[ocaml::func]
pub unsafe fn caml_rs_bls12_381_zero() -> ocaml::Pointer<CRFr> {
    ocaml::Pointer::alloc_custom(CRFr(Fr::from(0)))
}

#[ocaml::func]
pub unsafe fn caml_rs_bls12_381_one() -> ocaml::Pointer<CRFr> {
    ocaml::Pointer::alloc_custom(CRFr(Fr::from(1)))
}

#[ocaml::func]
pub unsafe fn caml_rs_bls12_381_eq(vx: ocaml::Pointer<CRFr>, vy: ocaml::Pointer<CRFr>) -> bool {
    let x = vx.as_ref().0;
    let y = vy.as_ref().0;
    x == y
}
