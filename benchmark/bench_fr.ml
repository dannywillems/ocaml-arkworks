module F = Arkworks.Fr

let test_addition =
  let e1 = F.random () in
  let e2 = F.random () in
  Core_bench.Bench.Test.create ~name:"Addition Fr" (fun () ->
      ignore (F.add e1 e2))

let () =
  let commands = [test_addition] in
  Command_unix.run (Core_bench.Bench.make_command commands)
