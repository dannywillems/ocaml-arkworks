# ocaml-arkworks

Binding to [arkworks](https://github.com/arkworks) libraries.

This is a WIP. Do not use.

## Benchmark

```
dune exec ./benchmark/bench_fr.exe
Done: 89% (17/19, 2 left) (jobs: 0)Estimated testing time 10s (1 benchmarks x 10s). Change using '-quota'.
┌─────────────┬──────────┬─────────┬────────────┐
│ Name        │ Time/Run │ mWd/Run │ Percentage │
├─────────────┼──────────┼─────────┼────────────┤
│ Addition Fr │  83.06ns │   6.00w │    100.00% │
└─────────────┴──────────┴─────────┴────────────┘
```
